FROM node:18

WORKDIR /var/www/app

COPY . .

RUN npm install

CMD [ "npm", "run", "dev" ]
