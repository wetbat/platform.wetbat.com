import React, { ReactNode, useState } from 'react';
import { ArrowRightOutlined, CaretRightOutlined, CloseCircleOutlined, CheckCircleOutlined, ExclamationCircleOutlined } from '@ant-design/icons';
import { Button, Collapse, theme, notification, Row, Col } from 'antd';
import { DrawerMenu } from '@/components/drawer-menu';
import { InvoiceService } from '@/services/invoice.service';

const Invoices: React.FC = (props) => {
  const [invoices, setInvoices] = useState<InvoiceService.FindAll.InvoiceDto[]>([])
  const { token } = theme.useToken();
  const [api, contextHolder] = notification.useNotification();

  async function confirmPayment(invoiceId: string) {
    const invoice = await InvoiceService.confirmPayment(invoiceId)
    api.success({
      message: `Success`,
      description: <p>Your trip is confirmed. Status: {invoice.status}</p>,
      placement: 'top',
      duration: 5
    });
    await findAll()
  }

  async function cancelPayment(invoiceId: string) {
    const invoice = await InvoiceService.cancelPayment(invoiceId)
    api.success({
      message: `Success`,
      description: <p>Your trip was canceled. Status: {invoice.status}</p>,
      placement: 'top',
      duration: 5
    });
    await findAll()
  }

  async function findAll() {
    InvoiceService.findAll().then(invoices => setInvoices(invoices))
  }

  function statusIcon(status: 'PENDING' | 'PAID' | 'CANCELED'): ReactNode {
    if(status === 'PAID') return <CheckCircleOutlined style={{ color: 'green' }} />
    if(status === 'CANCELED') return <CloseCircleOutlined style={{ color: 'red' }} />
    return <ExclamationCircleOutlined style={{ color: 'blue' }}/>
  }

  useState(() => {
    findAll()
  })

  return (
    <DrawerMenu selected='invoices' >
      {contextHolder}
      <Collapse
        bordered={false}
        defaultActiveKey={['1']}
        expandIcon={({ isActive }) => <CaretRightOutlined rotate={isActive ? 90 : 0} />}
        style={{ background: token.colorBgContainer }}
        items={invoices.map(invoice => ({
          key: invoice.id,
          label: <>
            {statusIcon(invoice.status)}
            {invoice.service.departure.airport.name}
            <ArrowRightOutlined />
            {invoice.service.arrival.airport.name}
          </>,
          children: <div>
            <p><strong>Status:</strong> {invoice.status}</p>
            <p><strong>Price:</strong> ${invoice.service.price}</p>
            <p><strong>Departure:</strong> {new Date(invoice.service.departure.date).toUTCString()}</p>
            <p><strong>Arrival:</strong> {new Date(invoice.service.arrival.date).toUTCString()}</p>
            <p><strong>Transportation:</strong> {invoice.service.transportation}</p>
            <p><strong>Travellers:</strong> {invoice.service.travalers}</p>
            <p><strong>Name:</strong> {invoice.service.contact.name}</p>
            <p><strong>Phone:</strong> {invoice.service.contact.phone}</p>
            <p><strong>E-mail:</strong> {invoice.service.contact.email}</p>
            <Row gutter={16}>
              <Col>
                {
                  invoice.status !== 'PAID' && <Button
                  type='primary'
                  htmlType='button'
                  style={{ background: '#5BBFBA' }}
                  onClick={() => confirmPayment(invoice.id)}
                >
                  Confirm Payment
                </Button>
                }
              </Col>
              {
                invoice.status !== 'CANCELED' && <Col>
                <Button
                  type='default'
                  htmlType='button'
                  onClick={() => cancelPayment(invoice.id)}
                >
                  Cancel Payment
                </Button>
              </Col>
              }
            </Row>
          </div>,
          style: {
            marginBottom: 24,
            background: token.colorFillAlter,
            borderRadius: token.borderRadiusLG,
            border: 'none',
          }
        }))}
      />
    </DrawerMenu>
  );
};

export default Invoices;