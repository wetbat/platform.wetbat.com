import React, { useState } from 'react';
import { ArrowRightOutlined, CaretRightOutlined } from '@ant-design/icons';
import { Collapse, theme } from 'antd';
import { DrawerMenu } from '@/components/drawer-menu';
import { ServiceService } from '@/services/service.service';

const Services: React.FC = (props) => {
  const [services, setServices] = useState<ServiceService.FindAll.ServiceDto[]>([])
  const { token } = theme.useToken();

  useState(() => {
    ServiceService.findAll().then(services => setServices(services))
  })

  return (
    <DrawerMenu selected='services' >
      <Collapse
        bordered={false}
        defaultActiveKey={['1']}
        expandIcon={({ isActive }) => <CaretRightOutlined rotate={isActive ? 90 : 0} />}
        style={{ background: token.colorBgContainer }}
        items={services.map(service => ({
          key: service.id,
          label: <>{service.departure.airport.name}<ArrowRightOutlined />{service.arrival.airport.name}</>,
          children: <div>
            <p><strong>Price:</strong> ${service.price}</p>
            <p><strong>Departure:</strong> {new Date(service.departure.date).toUTCString()}</p>
            <p><strong>Arrival:</strong> {new Date(service.arrival.date).toUTCString()}</p>
            <p><strong>Transportation:</strong> {service.transportation}</p>
            <p><strong>Travellers:</strong> {service.travalers}</p>
            <p><strong>Name:</strong> {service.contact.name}</p>
            <p><strong>Phone:</strong> {service.contact.phone}</p>
            <p><strong>E-mail:</strong> {service.contact.email}</p>
          </div>,
          style: {
            marginBottom: 24,
            background: token.colorFillAlter,
            borderRadius: token.borderRadiusLG,
            border: 'none',
          }
        }))}
      />
    </DrawerMenu>
  );
};

export default Services;