import React from 'react';
import { SearchFlytghForm } from '@/components/search-flyght.form';
import { DrawerMenu } from '@/components/drawer-menu';

const App: React.FC = (props) => {

  return (
    <DrawerMenu selected='home' >
      <SearchFlytghForm />
    </DrawerMenu>
  );
};

export default App;