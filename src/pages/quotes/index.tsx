import React, { useState } from 'react';
import { ArrowRightOutlined, CaretRightOutlined } from '@ant-design/icons';
import { Button, Collapse, theme, notification } from 'antd';
import { QuoteService } from '@/services/quote.service';
import { DrawerMenu } from '@/components/drawer-menu';
import { ServiceService } from '@/services/service.service';
import { Quote } from '@/components/quote';

const Quotes: React.FC = (props) => {
  const [quotes, setQuotes] = useState<QuoteService.FindAll.OutputDto[]>([])
  const { token } = theme.useToken();
  const [api, contextHolder] = notification.useNotification();

  async function convert (quoteId: string){
    const service = await ServiceService.convert(quoteId)
    api.success({
      message: `Success`,
      description: <p>Quote converted. Price: <strong>$ {service.price}</strong></p>,
      placement: 'top',
      duration: 5
    });
  }

  useState(() => {
    QuoteService.findAll().then(quotes => setQuotes(quotes))
  })

  return (
    <DrawerMenu selected='quotes' >
      {contextHolder}
      <Collapse
        bordered={false}
        defaultActiveKey={['1']}
        expandIcon={({ isActive }) => <CaretRightOutlined rotate={isActive ? 90 : 0} />}
        style={{ background: token.colorBgContainer }}
        items={quotes.map(quote => ({
          key: quote.id,
          label: <>{quote.departure.airport.name}<ArrowRightOutlined />{quote.arrival.airport.name}</>,
          children: <Quote quote={quote} />,
          style: {
            marginBottom: 24,
            background: token.colorFillAlter,
            borderRadius: token.borderRadiusLG,
            border: 'none',
          }
        }))}
      />
    </DrawerMenu>
  );
};

export default Quotes;