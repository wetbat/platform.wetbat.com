import React, { FC, PropsWithChildren } from "react"
import { DollarOutlined, BookOutlined, ScheduleOutlined, HomeOutlined } from '@ant-design/icons';
import { Layout, Menu, } from 'antd';
import Image from 'next/image'
import { useRouter } from 'next/navigation'

const { Header, Content, Footer, Sider } = Layout;

export const DrawerMenu: FC<PropsWithChildren<{ selected: 'home' | 'quotes' | 'services' | 'invoices' }>> = (props: PropsWithChildren<{ selected: string }>) => {
  const router = useRouter()
  return (
    <Layout style={{ height: '100vh' }}>
      <Sider
        breakpoint="lg"
        collapsedWidth="0"
        onBreakpoint={(broken) => {
          console.log(broken);
        }}
        onCollapse={(collapsed, type) => {
          console.log(collapsed, type);
        }}
      >
        <div className="demo-logo-vertical" style={{ height: 82 }}>
          <Image
            src="/wet-bat.png"
            width={178}
            height={82}
            alt="Picture of the author"
          />
        </div>
        <Menu
          theme="dark"
          mode="inline"
          defaultSelectedKeys={[props.selected]}
          items={[
            {
              key: 'home',
              icon: <HomeOutlined />,
              label: 'Home',
              onClick: () => router.push('/'),
              style: props.selected === 'home' ? { background: '#5BBFBA' } : {}
            },
            {
              key: 'quotes',
              icon: <BookOutlined />,
              label: 'Quotes',
              onClick: () => router.push('/quotes'),
              style: props.selected === 'quotes' ? { background: '#5BBFBA' } : {}
            },
            {
              key: 'services',
              icon: <ScheduleOutlined />,
              label: 'Services',
              onClick: () => router.push('/services'),
              style: props.selected === 'services' ? { background: '#5BBFBA' } : {}
            },
            {
              key: 'invoices',
              icon: <DollarOutlined />,
              label: 'Invoices',
              onClick: () => router.push('/invoices'),
              style: props.selected === 'invoices' ? { background: '#5BBFBA' } : {}
            }
          ]}
        />
      </Sider>
      <Layout>
        <Header style={{ padding: 0, }} />
        <Content style={{ margin: '24px 16px 0', overflowY: 'scroll' }}>
          {props.children}
        </Content>
        <Footer style={{ textAlign: 'center' }}>Paradoxo Desenvolvimento©2023</Footer>
      </Layout>
    </Layout>
  )
} 