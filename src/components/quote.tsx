import React from 'react'
import { Button, notification } from 'antd';
import { QuoteService } from '@/services/quote.service';
import { ServiceService } from '@/services/service.service';

interface InputDto {
  quote: QuoteService.FindAll.OutputDto
}

export const Quote: React.FC<React.PropsWithChildren<InputDto>> = (props: React.PropsWithChildren<InputDto>) => {
  const [api, contextHolder] = notification.useNotification();
  const [price, setPrice] = React.useState<number>()

  async function convert (quoteId: string){
    const service = await ServiceService.convert(quoteId)
    api.success({
      message: `Success`,
      description: <p>Quote converted. Price: <strong>$ {service.price}</strong></p>,
      placement: 'top',
      duration: 5
    });
  }

  async function getPrice(id: string) {
    const { price } = await QuoteService.getPrice({ id })
    setPrice(price)
  }

  return (
    <div>
      {contextHolder}
      <p><strong>Departure:</strong> {new Date(props.quote.departure.date).toUTCString()}</p>
      <p><strong>Arrival:</strong> {new Date(props.quote.arrival.date).toUTCString()}</p>
      <p><strong>Transportation:</strong> {props.quote.transportation}</p>
      <p><strong>Travellers:</strong> {props.quote.travalers}</p>
      <p><strong>Name:</strong> {props.quote.contact.name}</p>
      <p><strong>Phone:</strong> {props.quote.contact.phone}</p>
      <p><strong>E-mail:</strong> {props.quote.contact.email}</p>
      <Button
        type='primary'
        htmlType='button'
        style={{ background: '#5BBFBA' }}
        onClick={() => convert(props.quote.id)}
      >Convert</Button>
      <Button
        type='default'
        htmlType='button'
        style={{ marginLeft: 10 }}
        onClick={() => getPrice(props.quote.id)}
      >Get Price</Button>
      {price && <p><strong>$ {price}</strong></p>}
    </div>
  )
}