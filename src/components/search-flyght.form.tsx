import React, { useState, useEffect } from "react"
import { Card, Form, InputNumber, Row, Input, Select, Result, Button, DatePicker, Divider, notification } from "antd"
import { Moment } from "moment"
import { AirportService } from '@/services/airport.service';
import { QuoteService } from "@/services/quote.service";

const onFinishFailed = (errorInfo: any) => {
  console.log('Failed:', errorInfo);
};

type FieldType = {
  username?: string;
  password?: string;
  remember?: string;
  departureId?: string;
  arrivalId?: string;
  departureDate?: Date;
  travellers?: number;
  transportation?: string;
  name?: string;
  phone?: string;
  email?: string;
};

export const SearchFlytghForm: React.FC = (props) => {
  const [trip, setTrip] = useState<any>()
  const [readyToQuote, setReadyToQuote] = useState<boolean>(false)
  const [airports, setAirports] = useState<AirportService.Dto[]>([])
  const [api, contextHolder] = notification.useNotification();

  const onFinish = async (values: { arrivalId: string, departureId: string, transportation: string, travellers: number, departureDate: Moment, name: string, email: string, phone: string }) => {
    if(readyToQuote){
      await QuoteService.create({
        arrival: {
          airportId: values.arrivalId
        },
        contact: {
          email: values.email,
          name: values.name,
          phone: values.phone
        },
        departure: {
          airportId: values.departureId,
          date: values.departureDate.toDate()
        },
        travalers: values.travellers,
        transportation: values.transportation,
      })
      api.success({
        message: `Success`,
        description: <p>Your trip was quoted</p>,
        placement: 'top',
        duration: 5
      });
      setTrip(null)
      setReadyToQuote(false)
      return;
    }
    setTrip({
      travellers: values.travellers,
      arrivalId: values.arrivalId,
      departureId: values.departureId,
      transportation: values.transportation,
      departureDate: values.departureDate.toDate()
    })
  };

  useEffect(() => {
    AirportService.findAll()
      .then(airports => setAirports(airports))
      .catch(error => console.error({ error }))
  }, [])

  return (
    <>
      {contextHolder}
      <Card title="Earn 18 WETBAT Pass points for every dollar when you book your hotel" bordered={false}>
        <Form
          name="basic"
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          initialValues={{ remember: true }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
          layout='inline'
        >
          <Row>
            <Form.Item<FieldType>
              name="travellers"
            >
              <InputNumber style={{ width: 150 }} placeholder="Travellers" />
            </Form.Item>

            <Form.Item<FieldType>
              name="transportation"
            >
              <Input placeholder="Transportation" style={{ width: 150 }} />
            </Form.Item>

            <Form.Item<FieldType>
              name="departureDate"
            >
              <DatePicker placeholder="Date" style={{ width: 150 }} />
            </Form.Item>

            <Form.Item<FieldType>
              name="departureId"
            >
              <Select placeholder='Departure Airport' style={{ width: 350 }}>
                {
                  airports.map(airport => <Select.Option key={airport.id} value={airport.id}>{airport.name}</Select.Option>)
                }
              </Select>
            </Form.Item>

            <Form.Item<FieldType>
              name="arrivalId"
            >
              <Select placeholder="Arrival Airport" style={{ width: 350 }}>
                {
                  airports.map(airport => <Select.Option key={airport.id} value={airport.id}>{airport.name}</Select.Option>)
                }
              </Select>
            </Form.Item>

            {
              readyToQuote && (
                <>
                  <Divider />

                  <Form.Item<FieldType>
                    name="name"
                  >
                    <Input placeholder="Name" style={{ width: 300 }} />
                  </Form.Item>

                  <Form.Item<FieldType>
                    name="email"
                  >
                    <Input placeholder="E-mail" style={{ width: 300 }} />
                  </Form.Item>

                  <Form.Item<FieldType>
                    name="phone"
                  >
                    <Input placeholder="Phone" style={{ width: 300 }} />
                  </Form.Item>

                  <Divider />
                </>
              )
            }

            <Form.Item style={{ marginTop: readyToQuote ? 0 : 30 }} >
              <Button type="primary" htmlType="submit">
                { readyToQuote ? 'Finish Quote' : 'Search' }
              </Button>
            </Form.Item>
          </Row>
        </Form>
      </Card>
      {
        trip && <Result
          status="success"
          title="Flytgh Founded!"
          subTitle={`Origin: ${airports.find(airport => airport.id === trip.departureId)?.name} - Destination: ${airports.find(airport => airport.id === trip.arrivalId)?.name}`}
          extra={[
            <Button
              type="primary"
              key="console"
              onClick={() => {
                setTrip(null)
                setReadyToQuote(true)
              }}
            >
              Quote
            </Button>,
            <Button key="buy">Search Again</Button>,
          ]}
        />
      }
    </>
  )
}