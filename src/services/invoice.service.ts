import axios from 'axios'

export class InvoiceService {
	public static async confirmPayment(invoiceId: string): Promise<InvoiceService.ConfirmPayment.InvoiceDto> {
		const res = await axios.post<InvoiceService.ConfirmPayment.InvoiceDto>(`${process.env.NEXT_PUBLIC_API_URI}/invoices/confirm`, { id: invoiceId }, { headers: { 'Content-Type': 'application/json' } })
		const invoice = res.data
		return invoice
	}

	public static async cancelPayment(invoiceId: string): Promise<InvoiceService.CancelPayment.InvoiceDto> {
		const res = await axios.post<InvoiceService.CancelPayment.InvoiceDto>(`${process.env.NEXT_PUBLIC_API_URI}/invoices/cancel`, { id: invoiceId }, { headers: { 'Content-Type': 'application/json' } })
		const invoice = res.data
		return invoice
	}

	public static async findAll(): Promise<InvoiceService.FindAll.InvoiceDto[]> {
		const res = await axios.get<InvoiceService.FindAll.InvoiceDto[]>(`${process.env.NEXT_PUBLIC_API_URI}/invoices`, { headers: { 'Content-Type': 'application/json' } })
		const invoices = res.data
		return invoices
	}
}

export namespace InvoiceService {
	export namespace ConfirmPayment {
		export interface InvoiceDto {
			id: string
			serviceId: string
			status: 'PENDING' | 'PAID' | 'CANCELED'
			statusChangedAt: Date
		}
	}
	export namespace CancelPayment {
		export interface InvoiceDto {
			id: string
			serviceId: string
			status: 'PENDING' | 'PAID' | 'CANCELED'
			statusChangedAt: Date
		}
	}
	export namespace FindAll {
		export interface InvoiceDto {
			id: string
			serviceId: string
			status: 'PENDING' | 'PAID' | 'CANCELED'
			statusChangedAt: Date
			service: {
				id: string;
				price: number;
				orderedAt: Date;
				departure: {
					date: Date;
					airport: {
						name: string;
						address: {
							street: string;
							city: string;
							state: string;
							country: string;
						};
						location: {
							latitude: number;
							longitude: number;
						};
					};
				};
				arrival: {
					date: Date;
					airport: {
						name: string;
						address: {
							street: string;
							city: string;
							state: string;
							country: string;
						};
						location: {
							latitude: number;
							longitude: number;
						};
					};
				};
				travalers: number;
				transportation: string;
				contact: {
					name: string;
					phone: string;
					email: string;
				};
			};
		}
	}
}
