import axios from 'axios'

export class ServiceService {
	public static async convert(quoteId: string): Promise<ServiceService.Convert.ServiceDto> {
		const res = await axios.post<ServiceService.Convert.ServiceDto>(`${process.env.NEXT_PUBLIC_API_URI}/services/convert-quote`, { quoteId }, { headers: { 'Content-Type': 'application/json' } })
		const service = res.data
		return service
	}

	public static async findAll(): Promise<ServiceService.FindAll.ServiceDto[]> {
		const res = await axios.get<{ services: ServiceService.FindAll.ServiceDto[] }>(`${process.env.NEXT_PUBLIC_API_URI}/services`, { headers: { 'Content-Type': 'application/json' } })
		const data = res.data
		const services = data.services
		return services
	}
}

export namespace ServiceService {
	export namespace Convert {
		export interface ServiceDto {
			price: number
			orderedAt: Date
			arrival: {
				date: Date
				airport: {
					name: string
					address: {
						city: string
						country: string
						state: string
						street: string
					},
					location: {
						latitude: number
						longitude: number
					}
				}
			},
			departure: {
				date: Date
				airport: {
					name: string
					address: {
						city: string
						country: string
						state: string
						street: string
					},
					location: {
						latitude: number
						longitude: number
					}
				}
			},
			contact: {
				email: string
				name: string
				phone: string
			},
			id: string
			transportation: string,
			travalers: number
		}
	}
	export namespace FindAll {
		export interface ServiceDto {
			price: number
			orderedAt: Date
			arrival: {
				date: Date
				airport: {
					name: string
					address: {
						city: string
						country: string
						state: string
						street: string
					},
					location: {
						latitude: number
						longitude: number
					}
				}
			},
			departure: {
				date: Date
				airport: {
					name: string
					address: {
						city: string
						country: string
						state: string
						street: string
					},
					location: {
						latitude: number
						longitude: number
					}
				}
			},
			contact: {
				email: string
				name: string
				phone: string
			},
			id: string
			transportation: string,
			travalers: number
		}
	}
}
