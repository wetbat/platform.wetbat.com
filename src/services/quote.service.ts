import axios from 'axios'

export class QuoteService {
  public static async create(input: QuoteService.Create.InputDto): Promise<QuoteService.Create.OutputDto> {
    const res = await axios.post<QuoteService.Create.OutputDto>(`${process.env.NEXT_PUBLIC_API_URI}/quotes`, input)
    const quote = res.data
    return quote
  }

  public static async findAll(): Promise<QuoteService.FindAll.OutputDto[]> {
    const res = await axios.get<{ quotes: QuoteService.FindAll.OutputDto[] }>(`${process.env.NEXT_PUBLIC_API_URI}/quotes`)
    const data = res.data
    const quotes = data.quotes
    return quotes
  }

  public static async getPrice(input: QuoteService.GetPrice.InputDto): Promise<QuoteService.GetPrice.OutputDto> {
    const res = await axios.get<QuoteService.GetPrice.OutputDto>(`${process.env.NEXT_PUBLIC_API_URI}/quotes/price/${input.id}`)
    const price = res.data
    return price
  }
}

export namespace QuoteService {
  export namespace GetPrice {
    export interface InputDto {
      id: string
    }
    export interface OutputDto {
      price: number
    }
  }
  export namespace FindAll {
    export interface OutputDto {
      id: string
      departure: {
        airport: {
          id: string
          name: string
          address: {
            street: string
            city: string
            state: string
            country: string
          },
          location: {
            latitude: number,
            longitude: number
          }
        },
        date: Date
      },
      arrival: {
        airport: {
          id: string
          name: string
          address: {
            street: string
            city: string
            state: string
            country: string
          },
          location: {
            latitude: number
            longitude: number
          }
        },
        date: Date
      },
      transportation: string
      travalers: number
      contact: {
        name: string
        phone: string
        email: string
      }
    }
  }

  export namespace Create {
    export interface InputDto {
      departure: {
        date: Date;
        airportId: string;
      };
      arrival: {
        airportId: string;
      };
      travalers: number;
      transportation: string;
      contact: {
        name: string;
        phone: string;
        email: string;
      };
    }
    export interface OutputDto {
      departureDate: Date
      travellers: number
      transportation: string
      name: string
      email: string
      phone: string
      departureAirport: {
        location: {
          latitude: number;
          longitude: number;
        };
        address: {
          street: string;
          city: string;
          state: string;
          country: string;
        }
        name: string
      }
      arrivalAirport: {
        location: {
          latitude: number;
          longitude: number;
        };
        address: {
          street: string;
          city: string;
          state: string;
          country: string;
        }
        name: string
      }
    }
  }
}
