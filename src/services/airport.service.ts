import axios from 'axios'

export class AirportService {
  public static async findAll(): Promise<AirportService.Dto[]> {
    const res = await axios.get<{ airports: AirportService.Dto[] }>(`${process.env.NEXT_PUBLIC_API_URI}/airports`)
    const data = res.data
    const airports = data.airports
    return airports
  }
}

export namespace AirportService {
  export interface Dto {
    id: string
    name: string;
    location: {
      latitude: number;
      longitude: number;
    };
    address: {
      street: string;
      city: string;
      state: string;
      country: string;
    }
  }
}
